/*Released under GPL-3.0-only (read gpl-3.0.md)
Credits by Joseba Martinez (josebam@protonmail.com)
Copyright 2018,2019

This file is part of LifeGame.

LifeGame is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

LifeGame is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LifeGame.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStatusBar>
#include <QLabel>
#include <QTableWidget>
#include <string>
#include <QTimer>
#include <QPushButton>
#include <QInputDialog>
#include <fstream>

#include "matrixrep.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void play();
    
private slots:
    void on_pushButton_released();

    void on_pushButton_step_released();

    void on_pushButton_ppause_released();

    void on_spinBox_proportion_valueChanged(int arg1);

    void on_spinBox_born_valueChanged(int arg1);

    void on_spinBox_LiveMin_valueChanged(int arg1);

    void on_spinBox_LiveMax_valueChanged(int arg1);

    void on_pushButton_save_released();

    void on_pushButton_load_released();

    void on_doubleSpinBox_valueChanged(double arg1);

private:
    QTimer *timer;
    Ui::MainWindow *ui;
    int tableSize[2]={300,150};
    unsigned long stepNum=0;
    MatrixRep matrix;
    void drawMatrix();
    void writeDataToFile();
    bool debugTest=false;
    bool writeData=true;
    ofstream dataFile;
};

#endif // MAINWINDOW_H
