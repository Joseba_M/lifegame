/*Released under GPL-3.0-only (read gpl-3.0.md)
Credits by Joseba Martinez (josebam@protonmail.com)
Copyright 2018,2019

This file is part of LifeGame.

LifeGame is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

LifeGame is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LifeGame.  If not, see <https://www.gnu.org/licenses/>.*/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
