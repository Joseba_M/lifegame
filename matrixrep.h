/*Released under GPL-3.0-only (read gpl-3.0.md)
Credits by Joseba Martinez (josebam@protonmail.com)
Copyright 2018,2019

This file is part of LifeGame.

LifeGame is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

LifeGame is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LifeGame.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef MATRIXREP_H
#define MATRIXREP_H

#include <random>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

// Both values always bigger than 3
const int Xsize=200;
const int Ysize=100;

// reset probability centered
/* 0: NO
 * 1: romboid (-px-py)
 * 2: Rectangular (-2*fmax(px,py)
 * 3: circular sqrt(px²-py²)
*/
const float AdjustedProportion=0.525f;
const short centeredProv=3;

struct COORDINATES {
    int x;
    int y;
};

struct Matrix {
    bool b[Xsize][Ysize];
};

inline bool operator==(const Matrix &m, const Matrix &n)
{
    for(int i=0; i<Xsize; i++) {
        for(int j=0; j<Ysize; j++){
            if(m.b[i][j]!=n.b[i][j]) return false;
        }
    }
    return true;
}

class MatrixRep
{
private:
    int debug[2]={0,0};
    int toLiveMin=2;
    int toLiveMax=3;
    int born=3;

public:
    unsigned long totalLive=0;
    float proportion=AdjustedProportion;
    Matrix *field, *field0, *field1;
    bool writeData=false;

    MatrixRep();
    void resetField();
    // -1 if coo out of range
    // Counts how many live cells are inside
    int computeChunk(COORDINATES coo);
    // returns false if no changes are detected
    bool calculate();
    void calculateDebug();
    void setBorn(int n);
    void setMaxLive(int n);
    void setMinLive(int n);
    void saveToFile(string fName="lastReset.tmp");
    bool readFromFile(string fName="lastReset.tmp"); //false on error
    void writeDataFile(string fName="lastReset.csv");
};

class SubMatrix //TODO
{
private:

public:

};

#endif // MATRIXREP_H
