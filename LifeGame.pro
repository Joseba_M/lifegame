#Released under GPL-3.0-only (read gpl-3.0.md)
#Credits by Joseba Martinez (josebam@protonmail.com)
#Copyright 2018,2019

#This file is part of LifeGame.

#LifeGame is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License version 3 as published by
#the Free Software Foundation.

#LifeGame is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with LifeGame.  If not, see <https://www.gnu.org/licenses/>.


QT       += core gui widgets

TARGET = LifeGame
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    matrixrep.cpp

HEADERS += \
        mainwindow.h \
    matrixrep.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    LICENSE.txt \
    TODO.txt \
    gpl-3.0.md
