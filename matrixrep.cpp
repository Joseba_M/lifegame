/*Released under GPL-3.0-only (read gpl-3.0.md)
Credits by Joseba Martinez (josebam@protonmail.com)
Copyright 2018,2019

This file is part of LifeGame.

LifeGame is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

LifeGame is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LifeGame.  If not, see <https://www.gnu.org/licenses/>.*/

#include "matrixrep.h"

MatrixRep::MatrixRep()
{
    resetField();
}

void MatrixRep::resetField()
{
    field=new Matrix;
    field0=new Matrix;
    field1=new Matrix;
    totalLive=0;
    std::random_device rd;
    std::uniform_real_distribution<float> dist(0.0f,1.0f);
    // Generate field inicial random
    for(int i=0; i<Xsize; i++) {
        for(int j=0; j<Ysize; j++) {
            float p=dist(rd);
            switch (centeredProv) {
            case 1: p=p*proportion-abs(-0.5f+float(i)/Xsize)-abs(-0.5f+float(j)/Ysize); break;
            case 2: p=p*proportion-2.0f*fmax(abs(-0.5f+float(i)/Xsize),abs(-0.5f+float(j)/Ysize)); break;
            case 3: p=p*proportion-sqrt(pow(-0.5f+float(i)/Xsize,2.0f)+pow(-0.5f+float(j)/Ysize,2.0f)); break;
            default: p=p*proportion;break;
            }
            field->b[i][j]=p>=0.5f;
            if(p>0.5f) totalLive++;
        }
    }
    debug[0]=0;debug[1]=0;
    *field0=*field;
    field1->b[1][1]=true;
}

int MatrixRep::computeChunk(COORDINATES coo)
{
    if(coo.x>=Xsize || coo.y>=Ysize) return -1; //error
    // Create chunk coordinates
    COORDINATES c[3][3];
    for(int a=0;a<3;a++) { //Diagonal values
        c[1][a].x=coo.x; c[a][1].y=coo.y;
    }
    // x values
    if(coo.x==0) {c[0][0].x=Xsize-1; c[0][1].x=Xsize-1; c[0][2].x=Xsize-1;}
    else {c[0][0].x=coo.x-1; c[0][1].x=coo.x-1; c[0][2].x=coo.x-1;}
    if(coo.x==Xsize-1) {c[2][0].x=0; c[2][1].x=0; c[2][2].x=0;}
    else {c[2][0].x=coo.x+1; c[2][1].x=coo.x+1; c[2][2].x=coo.x+1;}

    // y values
    if(coo.y==0) {c[0][0].y=Ysize-1; c[1][0].y=Ysize-1; c[2][0].y=Ysize-1;}
    else {c[0][0].y=coo.y-1; c[1][0].y=coo.y-1; c[2][0].y=coo.y-1;}
    if(coo.y==Ysize-1) {c[0][2].y=0; c[1][2].y=0; c[2][2].y=0;}
    else {c[0][2].y=coo.y+1; c[1][2].y=coo.y+1; c[2][2].y=coo.y+1;}

    int count=0;
    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++) {
            if(field0->b[c[i][j].x][c[i][j].y]) count++;
        }
    }
    if(field0->b[coo.x][coo.y]) count--;
    return count;
}

bool MatrixRep::calculate()
{
    totalLive=0;
    bool changed=true;
    COORDINATES coo{0,0};
    for(int i=0; i<Xsize; i++) {
        for(int j=0; j<Ysize; j++) {
            coo={i,j};
            int val=computeChunk(coo);
            bool f=field0->b[i][j];
            if(f && val>=toLiveMin && val<=toLiveMax) {field->b[i][j]=true; totalLive++;}
            else if (!f && val==born) {field->b[i][j]=true; totalLive++;}
            else field->b[i][j]=false;
        }
    }

    if(*field0==*field || *field1==*field) changed=false;

    *field1=*field0;
    *field0=*field;

    return changed;
}

void MatrixRep::calculateDebug()
{
    totalLive=0;
    for(int i=0; i<Xsize; i++) {
        for(int j=0; j<Ysize; j++) {
            field->b[i][j]=false;
        }
    }
    COORDINATES coo={debug[0],debug[1]};
    COORDINATES c[3][3];
    for(int a=0;a<3;a++) { //Diagonal values
        c[1][a].x=coo.x; c[a][1].y=coo.y;
    }
    // x values
    if(coo.x==0) {c[0][0].x=Xsize-1; c[0][1].x=Xsize-1; c[0][2].x=Xsize-1;}
    else {c[0][0].x=coo.x-1; c[0][1].x=coo.x-1; c[0][2].x=coo.x-1;}
    if(coo.x==Xsize-1) {c[2][0].x=0; c[2][1].x=0; c[2][2].x=0;}
    else {c[2][0].x=coo.x+1; c[2][1].x=coo.x+1; c[2][2].x=coo.x+1;}

    // y values
    if(coo.y==0) {c[0][0].y=Ysize-1; c[1][0].y=Ysize-1; c[2][0].y=Ysize-1;}
    else {c[0][0].y=coo.y-1; c[1][0].y=coo.y-1; c[2][0].y=coo.y-1;}
    if(coo.y==Ysize-1) {c[0][2].y=0; c[1][2].y=0; c[2][2].y=0;}
    else {c[0][2].y=coo.y+1; c[1][2].y=coo.y+1; c[2][2].y=coo.y+1;}

    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++) {
            field->b[c[i][j].x][c[i][j].y]=true;
        }
    }
    if(debug[0]>=Xsize-1) {
        debug[0]=0; debug[1]++;
    }
    else if(debug[1]>Ysize-1) {
        debug[0]++;
        debug[1]=0;
    }
    else debug[0]++;
    if(debug[0]>=9&&debug[1]>=9) {
        debug[0]=0; debug[1]=0;
    }
}

void MatrixRep::setBorn(int n)
{
    if(n>8) n=8;
    born=n;
}

void MatrixRep::setMaxLive(int n)
{
    if(n>8) n=8;
    toLiveMax=n;
}

void MatrixRep::setMinLive(int n)
{
    if(n>8) n=8;
    toLiveMin=n;
}

void MatrixRep::saveToFile(string fName)
{
    remove(fName.c_str());
    ofstream f;
    f.open(fName.c_str());
    f<<Xsize<<endl<<Ysize<<endl<<"0"<<endl<<"0"<<endl;
    for(int j=0; j<Ysize; j++) {
        for(int i=0; i<Xsize; i++) {
            if(field->b[i][j]) f<<"1";
            else            f<<"0";
        }
        f<<endl;
    }
}

bool MatrixRep::readFromFile(string fName) //only valid for full matrix loading
{
    ifstream f;
    f.open(fName.c_str());
    if(!f.is_open()) return false;

    string line="";
    int xy[4];

    for(int a=0; a<4; a++) {
        getline(f,line);
        try {xy[a]=stoi(line);} catch(exception &err) {return false;}
    }
    if(xy[0]!=Xsize || xy[1]!=Ysize) return false;

    for(int j=0; j<Ysize; j++) {
        getline(f,line);
        if(line.length()!=Xsize) return false;
        for(int i=0; i<Xsize; i++) {
            if(line.at(i)=='1') field0->b[i][j]=true;
            else                field0->b[i][j]=false;
        }
    }
    for(int i=0; i<Xsize; i++) {
        for(int j=0; j<Ysize; j++) {
            field->b[i][j]=field0->b[i][j];
        }
    }
    return true;
}
