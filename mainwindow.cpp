/*Released under GPL-3.0-only (read gpl-3.0.md)
Credits by Joseba Martinez (josebam@protonmail.com)
Copyright 2018,2019

This file is part of LifeGame.

LifeGame is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

LifeGame is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LifeGame.  If not, see <https://www.gnu.org/licenses/>.*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    remove("lastData.csv");
    dataFile.open("lastData.csv");
    timer=new QTimer;
    timer->setInterval(500);
    ui->setupUi(this);
    ui->statusBar->showMessage("Starting...");
    tableSize[0]=Xsize;
    tableSize[1]=Ysize;
    ui->tableWidget->setColumnCount(tableSize[0]);
    ui->tableWidget->setRowCount(tableSize[1]);
    ui->label_step->setText("Step: 0");
    for(int i=0; i<Xsize; i++) {
        for(int j=0; j<Ysize; j++) {
            ui->tableWidget->setItem(j,i,new QTableWidgetItem);
        }
    }
    ui->tableWidget->setShowGrid(false);
    drawMatrix();
    QObject::connect(timer, &QTimer::timeout, this, &MainWindow::play);
}

MainWindow::~MainWindow()
{
    dataFile.close();
    delete ui;
}

void MainWindow::play()
{
    stepNum++;
    QString str("Step: ");
    str+=QString::number(stepNum);
    ui->label_step->setText(str);
    if(!debugTest) {
        if(!matrix.calculate()) {
            ui->statusBar->showMessage("No changes detected!");
            timer->stop();
            ui->pushButton_ppause->setText("Pause");
        }
    }
    else matrix.calculateDebug(); // For debug
    drawMatrix();
    if(writeData) {
        dataFile<<stepNum<<"\t"<<matrix.totalLive<<endl;
    }
}

void MainWindow::on_pushButton_released()
{
    dataFile.close();
    remove("lastData.csv");
    dataFile.open("lastData.csv");
    matrix.resetField();
    matrix.saveToFile();
    ui->statusBar->showMessage("Restaring...");
    ui->pushButton_ppause->setText("Play");
    stepNum=0;
    ui->label_step->setText("Step: 0");
    drawMatrix();
}

void MainWindow::on_pushButton_step_released()
{
    play();
}

void MainWindow::drawMatrix()
{
    for(int i=0; i<tableSize[0]; i++) {
        for(int j=0; j<tableSize[1]; j++) {
            if(matrix.field->b[i][j])   ui->tableWidget->item(j,i)->setBackground(Qt::black);
            else                        ui->tableWidget->item(j,i)->setBackground(Qt::white);
        }
    }
    QString s="Cells: ";
    ui->label_cell->setText((s+QString::number(matrix.totalLive)));
}

void MainWindow::writeDataToFile()
{

}

void MainWindow::on_pushButton_ppause_released()
{
    if(!timer->isActive()) {
        ui->pushButton_ppause->setText("Pause");
        timer->start();
    }
    else {
        ui->pushButton_ppause->setText("Play");
        timer->stop();
    }
}

void MainWindow::on_spinBox_proportion_valueChanged(int arg1)
{
    matrix.proportion=0.5f+(arg1/200.0f);
}

void MainWindow::on_spinBox_born_valueChanged(int arg1)
{
    matrix.setBorn(arg1);
}

void MainWindow::on_spinBox_LiveMin_valueChanged(int arg1)
{
    matrix.setMinLive(arg1);
}

void MainWindow::on_spinBox_LiveMax_valueChanged(int arg1)
{
    matrix.setMaxLive(arg1);
}

void MainWindow::on_pushButton_save_released()
{
    QString readed="";
    readed=QInputDialog::getText(this, QString("Save As..."), QString("Save name:"));
    if(readed=="") matrix.saveToFile();
    else matrix.saveToFile(readed.toStdString());
    ui->statusBar->showMessage("File saved...");
}

void MainWindow::on_pushButton_load_released()
{
    bool ok=false;
    QString readed="";
    readed=QInputDialog::getText(this, QString("Load..."), QString("Load file name:"));
    if(readed=="") ok=matrix.readFromFile();
    else ok=matrix.readFromFile(readed.toStdString());
    if(ok) {
        ui->statusBar->showMessage("File loaded...");
        drawMatrix();
    }
    else ui->statusBar->showMessage("Loading error");
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    timer->setInterval(int(arg1)*1000);
}
